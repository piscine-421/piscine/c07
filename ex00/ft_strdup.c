/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/28 21:29:24 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
//#include <stdio.h>

char	*ft_strcpy(char *dest, char *src)
{
	int		index;

	index = 0;
	while (src[index] != '\0')
	{
		dest[index] = src[index];
		index++;
	}
	dest[index] = src[index];
	return (dest);
}

char	*ft_strdup(char *src)
{
	char	*string;
	int		index;

	index = 0;
	while (src[index])
		index++;
	string = malloc((index + 1) * sizeof(char));
	if (string == 0)
		return (0);
	ft_strcpy(string, src);
	return (string);
}

/*int	main(void)
{
	printf("%s\n", ft_strdup("please work please work please work"));
}*/
