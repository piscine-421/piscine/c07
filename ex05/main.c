/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/10/03 21:00:56 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

char	**ft_split(char *str, char *charset);

int	ft_strcmp(char *s1, char *s2)
{
	int	index;

	index = 0;
	while (s1[index] == s2[index])
	{
		if (s1[index] == '\0')
			return (0);
		index++;
	}
	return (s1[index] - s2[index]);
}

int	main(void)
{
	int		i;
	int		fail;
	char	*string;
	char	*sep;
	char	**result;

	i = 0;
	fail = 0;

	// Test 1
	string = "                       ";
	sep = "     ";
	result = malloc(8);
	result[0] = 0;
	printf("\033[1;32m");
	while (ft_split(string, sep)[i])
	{
		if (ft_strcmp(ft_split(string, sep)[i], result[i]) != 0)
		{
			printf("\033[1;31m");
			fail = 1;
		}
		printf("ft_split tab[%d]: %s\n", i, ft_split(string, sep)[i]);
		printf("expected tab[%d]: %s\n", i, result[i]);
		i++;
	}
	if (fail == 1)
		printf("\033[1;31m\nTest 1 Failed\n\n");
	else
		printf("\033[1;32m\nTest 1 Passed\n\n");
	i = 0;
	fail = 0;

	// Test 2
	string = "                                       ";
	sep = "     ";
	result = malloc(8);
	result[0] = 0;
	printf("\033[1;32m");
	while (ft_split(string, sep)[i])
	{
		if (ft_strcmp(ft_split(string, sep)[i], result[i]) != 0)
		{
			printf("\033[1;31m");
			fail = 1;
		}
		printf("ft_split tab[%d]: %s\n", i, ft_split(string, sep)[i]);
		printf("expected tab[%d]: %s\n", i, result[i]);
		i++;
	}
	if (fail == 1)
		printf("\033[1;31mTest 2 Failed\n\n");
	else
		printf("\033[1;32mTest 2 Passed\n\n");
	i = 0;
	fail = 0;

	// Test 3
	string = "zKaEd7V2";
	sep = "zKaEd7V2";
	result = malloc(8);
	result[0] = 0;
	printf("\033[1;32m");
	while (ft_split(string, sep)[i])
	{
		if (ft_strcmp(ft_split(string, sep)[i], result[i]) != 0)
		{
			printf("\033[1;31m");
			fail = 1;
		}
		printf("ft_split tab[%d]: %s\n", i, ft_split(string, sep)[i]);
		printf("expected tab[%d]: %s\n", i, result[i]);
		i++;
	}
	if (fail == 1)
		printf("\033[1;31mTest 3 Failed\n\n");
	else
		printf("\033[1;32mTest 3 Passed\n\n");
	i = 0;
	fail = 0;

	// Test 4
	string = "IZa4sXP";
	sep = "IZa4sXP";
	result = malloc(8);
	result[0] = 0;
	printf("\033[1;32m");
	while (ft_split(string, sep)[i])
	{
		if (ft_strcmp(ft_split(string, sep)[i], result[i]) != 0)
		{
			printf("\033[1;31m");
			fail = 1;
		}
		printf("ft_split tab[%d]: %s\n", i, ft_split(string, sep)[i]);
		printf("expected tab[%d]: %s\n", i, result[i]);
		i++;
	}
	if (fail == 1)
		printf("\033[1;31mTest 4 Failed\n\n");
	else
		printf("\033[1;32mTest 4 Passed\n\n");
	i = 0;
	fail = 0;

	// Test 5
	string = "d6oGznOQ7UO0V1kGEub4tcNy29eBlBnKhKahN9Pg";
	sep = "ecp35K";
	result = malloc(48);
	result[0] = "d6oGznOQ7UO0V1kGEub4t";
	result[1] = "Ny29";
	result[2] = "BlBn";
	result[3] = "h";
	result[4] = "ahN9Pg";
	result[5] = 0;
	printf("\033[1;32m");
	while (ft_split(string, sep)[i])
	{
		if (ft_strcmp(ft_split(string, sep)[i], result[i]) != 0)
		{
			printf("\033[1;31m");
			fail = 1;
		}
		printf("ft_split tab[%d]: %s\n", i, ft_split(string, sep)[i]);
		printf("expected tab[%d]: %s\n", i, result[i]);
		i++;
	}
	if (fail == 1)
		printf("\033[1;31mTest 5 Failed\n\n");
	else
		printf("\033[1;32mTest 5 Passed\n\n");
	i = 0;
	fail = 0;

	// Test 6
	string = "t3wQhTJqvSEoBw2FGkx4u KNerhZPGs";
	sep = "OT";
	result = malloc(24);
	result[0] = "t3wQh";
	result[1] = "JqvSEoBw2FGkx4u KNerhZPGs";
	result[2] = 0;
	printf("\033[1;32m");
	while (ft_split(string, sep)[i])
	{
		if (ft_strcmp(ft_split(string, sep)[i], result[i]) != 0)
		{
			printf("\033[1;31m");
			fail = 1;
		}
		printf("ft_split tab[%d]: %s\n", i, ft_split(string, sep)[i]);
		printf("expected tab[%d]: %s\n", i, result[i]);
		i++;
	}
	if (fail == 1)
		printf("\033[1;31mTest 6 Failed\n\n");
	else
		printf("\033[1;32mTest 6 Passed\n\n");
	i = 0;
	fail = 0;

	// Test 7
	string = "isz81SpqXqX8J519XiBKx8rrUfJ rqMPwCrPMrPK";
	sep = "GCzu0mjw";
	result = malloc(32);
	result[0] = "is";
	result[1] = "81SpqXqX8J519XiBKx8rrUfJ rqMP";
	result[2] = "rPMrPK";
	result[3] = 0;
	printf("\033[1;32m");
	while (ft_split(string, sep)[i])
	{
		if (ft_strcmp(ft_split(string, sep)[i], result[i]) != 0)
		{
			printf("\033[1;31m");
			fail = 1;
		}
		printf("ft_split tab[%d]: %s\n", i, ft_split(string, sep)[i]);
		printf("expected tab[%d]: %s\n", i, result[i]);
		i++;
	}
	if (fail == 1)
		printf("\033[1;31mTest 7 Failed\n\n");
	else
		printf("\033[1;32mTest 7 Passed\n\n");
	i = 0;
	fail = 0;

	// Test 8
	string = "HoiXd1rAeARXb1OliJO2ThRKHeQzIwsy";
	sep = "5NFqSie";
	result = malloc(48);
	result[0] = "Ho";
	result[1] = "Xd1rA";
	result[2] = "ARXb1Ol";
	result[3] = "JO2ThRKH";
	result[4] = "QzIwsy";
	result[5] = 0;
	printf("\033[1;32m");
	while (ft_split(string, sep)[i])
	{
		if (ft_strcmp(ft_split(string, sep)[i], result[i]) != 0)
		{
			printf("\033[1;31m");
			fail = 1;
		}
		printf("ft_split tab[%d]: %s\n", i, ft_split(string, sep)[i]);
		printf("expected tab[%d]: %s\n", i, result[i]);
		i++;
	}
	if (fail == 1)
		printf("\033[1;31mTest 8 Failed\n\n");
	else
		printf("\033[1;32mTest 8 Passed\n\n");
	i = 0;
	fail = 0;

	// Test 9
	string = "vqWMrXmEQXsSuIzUqDavptIoWrVE4R1wVVwct48Zoi4CshB";
	sep = "sl64";
	result = malloc(56);
	result[0] = "vqWMrXmEQX";
	result[1] = "SuIzUqDavptIoWrVE";
	result[2] = "R1wVVwct";
	result[3] = "8Zoi";
	result[4] = "C";
	result[5] = "hB";
	result[6] = 0;
	printf("\033[1;32m");
	while (ft_split(string, sep)[i])
	{
		if (ft_strcmp(ft_split(string, sep)[i], result[i]) != 0)
		{
			printf("\033[1;31m");
			fail = 1;
		}
		printf("ft_split tab[%d]: %s\n", i, ft_split(string, sep)[i]);
		printf("expected tab[%d]: %s\n", i, result[i]);
		i++;
	}
	if (fail == 1)
		printf("\033[1;31mTest 9 Failed\n\n");
	else
		printf("\033[1;32mTest 9 Passed\n\n");
	i = 0;
	fail = 0;

	// Test 10
	string = "Ed6YzA p27sNHB6zgv1EFGz9F27W46AcmjIXzRUb0";
	sep = "gj3";
	result = malloc(32);
	result[0] = "Ed6YzA p27sNHB6z";
	result[1] = "v1EFGz9F27W46Acm";
	result[2] = "IXzRUb0";
	result[3] = 0;
	printf("\033[1;32m");
	while (ft_split(string, sep)[i])
	{
		if (ft_strcmp(ft_split(string, sep)[i], result[i]) != 0)
		{
			printf("\033[1;31m");
			fail = 1;
		}
		printf("ft_split tab[%d]: %s\n", i, ft_split(string, sep)[i]);
		printf("expected tab[%d]: %s\n", i, result[i]);
		i++;
	}
	if (fail == 1)
		printf("\033[1;31mTest 10 Failed\n\n");
	else
		printf("\033[1;32mTest 10 Passed\n\n");
	i = 0;
	fail = 0;

	// Test 11
	string = "  gh  ";
	sep = " gh";
	result = malloc(8);
	result[0] = 0;
	printf("\033[1;32m");
	while (ft_split(string, sep)[i])
	{
		if (ft_strcmp(ft_split(string, sep)[i], result[i]) != 0)
		{
			printf("\033[1;31m");
			fail = 1;
		}
		printf("ft_split tab[%d]: %s\n", i, ft_split(string, sep)[i]);
		printf("expected tab[%d]: %s\n", i, result[i]);
		i++;
	}
	if (fail == 1)
		printf("\033[1;31mTest 11 Failed\n\n");
	else
		printf("\033[1;32mTest 11 Passed\n\n");
	i = 0;
	fail = 0;

	// Test 12
	string = "  gh  ";
	sep = " ";
	result = malloc(16);
	result[0] = "gh";
	result[1] = 0;
	printf("\033[1;32m");
	while (ft_split(string, sep)[i])
	{
		if (ft_strcmp(ft_split(string, sep)[i], result[i]) != 0)
		{
			printf("\033[1;31m");
			fail = 1;
		}
		printf("ft_split tab[%d]: %s\n", i, ft_split(string, sep)[i]);
		printf("expected tab[%d]: %s\n", i, result[i]);
		i++;
	}
	if (fail == 1)
		printf("\033[1;31mTest 12 Failed\n\n");
	else
		printf("\033[1;32mTest 12 Passed\n\n");
	i = 0;
	fail = 0;

	// Test 12
	string = "  gh  ";
	sep = " ";
	result = malloc(16);
	result[0] = "gh";
	result[1] = 0;
	printf("\033[1;32m");
	while (ft_split(string, sep)[i])
	{
		if (ft_strcmp(ft_split(string, sep)[i], result[i]) != 0)
		{
			printf("\033[1;31m");
			fail = 1;
		}
		printf("ft_split tab[%d]: %s\n", i, ft_split(string, sep)[i]);
		printf("expected tab[%d]: %s\n", i, result[i]);
		i++;
	}
	if (fail == 1)
		printf("\033[1;31mTest 12 Failed\n\n");
	else
		printf("\033[1;32mTest 12 Passed\n\n");
	i = 0;
	fail = 0;

	// Test 13
	string = "tCCwZmd8w eImjooVqVoLYyB7 TBpx L";
	sep = "";
	result = malloc(16);
	result[0] = "tCCwZmd8w eImjooVqVoLYyB7 TBpx L";
	result[1] = 0;
	printf("\033[1;32m");
	while (ft_split(string, sep)[i])
	{
		if (ft_strcmp(ft_split(string, sep)[i], result[i]) != 0)
		{
			printf("\033[1;31m");
			fail = 1;
		}
		printf("ft_split tab[%d]: %s\n", i, ft_split(string, sep)[i]);
		printf("expected tab[%d]: %s\n", i, result[i]);
		i++;
	}
	if (fail == 1)
		printf("\033[1;31mTest 13 Failed\n\n");
	else
		printf("\033[1;32mTest 13 Passed\n\n");
	i = 0;
	fail = 0;

	// Test 14
	string = "MQrO5WUj XMMioP55tKTA8 G78z    m8uwZirP0";
	sep = "mA";
	result = malloc(16);
	result[0] = "MQrO5WUj XMMioP55tKT";
	result[1] = "8 G78z ";
	result[2] = "8uwZirP0";
	result[3] = 0;
	printf("\033[1;32m");
	while (ft_split(string, sep)[i])
	{
		if (ft_strcmp(ft_split(string, sep)[i], result[i]) != 0)
		{
			printf("\033[1;31m");
			fail = 1;
		}
		printf("ft_split tab[%d]: %s\n", i, ft_split(string, sep)[i]);
		printf("expected tab[%d]: %s\n", i, result[i]);
		i++;
	}
	if (fail == 1)
		printf("\033[1;31mTest 13 Failed\n\n");
	else
		printf("\033[1;32mTest 13 Passed\n\n");

	printf("\033[1;0m");
}
