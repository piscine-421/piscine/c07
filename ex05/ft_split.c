/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/10/03 21:00:56 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	sep(char c, char *charset)
{
	int	i;

	i = 0;
	while (charset[i])
	{
		if (charset[i] == c)
			return (1);
		i++;
	}
	if (c == '\0')
		return (1);
	return (0);
}

void	loop(int *i, char *str, char *charset, char **returned)
{
	while (str[i[0]])
	{
		i[1] = i[0];
		if (sep(str[i[0]], charset) == 0 && sep(str[i[0] + 1], charset) == 1)
		{
			while (sep(str[i[1]], charset) == 0 && i[1] >= 0)
			{
				i[1]--;
				i[3]++;
			}
			i[3]++;
			i[1] += i[3] + 1;
			returned[i[2]] = malloc((i[3] + 1) * sizeof(char));
			returned[i[2]][i[3] + 1] = '\0';
			while (i[3] >= 0)
			{
				if (sep(str[i[1]], charset) == 0)
					returned[i[2]][i[3]] = str[i[1]];
				i[1]--;
				i[3]--;
			}
			i[2]++;
		}
		i[0]++;
	}
}

int	wordcount(char *str, char *charset)
{
	int	i;
	int	wc;

	i = 0;
	wc = 0;
	while (str[i])
	{
		if (sep(str[i], charset) == 0 && sep(str[i + 1], charset) == 1)
			wc++;
		i++;
	}
	return (wc);
}

char	**ft_split(char *str, char *charset)
{
	int		i[4];
	char	**returned;

	i[0] = 0;
	i[2] = 0;
	i[3] = 0;
	returned = malloc(((wordcount(str, charset)) + 1) * 8);
	loop(i, str, charset, returned);
	returned[i[2]] = malloc(1);
	returned[i[2]] = 0;
	return (returned);
}
