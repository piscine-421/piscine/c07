# C07

Functions created for the 42 C Piscine project C07. All files have a main function commented out for quick testing except ex05, which has a separate main.c.

* ex00/ft_strdup reimplements the libc function strdup.
* ex01/ft_range creates an integer array containing all numbers between min and max, max excluded.
* ex02/ft_ultimate_range is the same as ft_range, except the integer array is given as a parameter instead of being the return value.
* ex03/ft_strjoin creates a string combining every string from a string array separated by a given separator string.
* ex04/ft_convert_base converts a number from one base to another.
* ex05/ft_split separates a string into a string array with seperator characters delimitting the parts of the original string that need to be split off.

ex00-ex04 have been verified working by the moulinette. ex05 fails for unknown reasons. My final score for this project as graded by the moulinette is 80/100.
